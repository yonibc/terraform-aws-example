# initialize variables (template.terraform.tfvars => terraform.tfvars)
variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "SSH_PUB_KEY" {}

# Specify the provider and access details
provider "aws" {
  region = "eu-central-1"
  access_key = "${var.AWS_ACCESS_KEY}"
  secret_key = "${var.AWS_SECRET_KEY}"
}

# var availability_zone
variable "desired_availability_zone" {
  default = "eu-central-1a"
}

################
# SSH key pair #
################
# create a ssh keypair
resource "aws_key_pair" "pubkey" {
  key_name   = "my-pubkey"
  public_key = "${var.SSH_PUB_KEY}"
}

###########
# Network #
###########
# add vpc
resource "aws_vpc" "vpc_terraform" {
  cidr_block = "10.99.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true
  tags = {
    Name = "VPC Terraform"
  }
}

# add subnet (public)
resource "aws_subnet" "subnet_public_terraform" {
  vpc_id = "${aws_vpc.vpc_terraform.id}"
  cidr_block ="10.99.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone = "${var.desired_availability_zone}"
  tags = {
    Name = "Subnet (public) Terraform"
  }
}

# add internet gateway
resource "aws_internet_gateway" "gw_terraform" {
  vpc_id = "${aws_vpc.vpc_terraform.id}"
  tags = {
        Name = "Internet Gateway Terraform"
    }
}

# create a route table for internet access
resource "aws_route" "internet_access_terraform" {
  route_table_id         = "${aws_vpc.vpc_terraform.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.gw_terraform.id}"
}

# Associate subnet (public) to route table for internet access
resource "aws_route_table_association" "public_subnet_association_terraform" {
    subnet_id = "${aws_subnet.subnet_public_terraform.id}"
    route_table_id = "${aws_vpc.vpc_terraform.main_route_table_id}"
}

#############################
# Security (Network) #
#############################
# create inbound sec group for http
resource "aws_security_group" "sec_group_http80_terraform" {
  name = "Allow HTTP 80"
  tags = {
        Name = "Allow HTTP 80"
  }
  description = "Allow HTTP 80 INBOUD"
  vpc_id = "${aws_vpc.vpc_terraform.id}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

# create inbound sec group for ssh
resource "aws_security_group" "sec_group_ssh22_terraform" {
  name = "Allow SSH 22"
  tags = {
        Name = "Allow SSH 22"
  }
  description = "Allow SSH 22 INBOUD"
  vpc_id = "${aws_vpc.vpc_terraform.id}"

  ingress {
    from_port   = "22"
    to_port     = "22"
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"] # optional: you can change this to your public IP/IP-Range
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  lifecycle {
    create_before_destroy = true
  }
}

################
# EC2 instance #
################
# create ec2 instance from ami
resource "aws_instance" "ec2_terraform" {
  # search ami ids => https://cloud-images.ubuntu.com/locator/ec2/
  ami           = "ami-df8406b0" # ami for you region
  instance_type = "t2.micro" # you may choose if you are on free tier: "Free tier eligible"
  subnet_id     = "${aws_subnet.subnet_public_terraform.id}"
  key_name      = "${aws_key_pair.pubkey.key_name}"
  associate_public_ip_address = true
  security_groups = ["${aws_security_group.sec_group_http80_terraform.id}","${aws_security_group.sec_group_ssh22_terraform.id}"]
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name        = "Terraform-Test"
  }
  # using cloud init => https://cloudinit.readthedocs.io/en/latest/
  user_data     = "${file("user-data.yml")}"
}

#####################
# Output (optional) #
#####################
# availability zone
output "availability_zone-name" {
  value = "${var.desired_availability_zone}"
}

# vpc id
output "vpc-id" {
  value = "${aws_vpc.vpc_terraform.id}"
}

# subnet id
output "subnet-id" {
  value = "${aws_subnet.subnet_public_terraform.id}"
}

# internet gateway id
output "gw-id" {
  value = "${aws_internet_gateway.gw_terraform.id}"
}

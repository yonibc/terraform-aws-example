# README #

### What is this repository for? ###

Simple [Terraform](https://www.terraform.io/) Konfiguration zum erstellen einer EC2 Instanz (Ubuntu 16.04) samt "einfachem" Netzwerk. Für die EC2 kommt zusätzlich noch [cloud-init](https://cloudinit.readthedocs.io/en/latest/) zur Initialisierung zum Einsatz.

Schritte:

* Netzwerk & Routing & FW-Regeln erstellen
    * Wir erstellen ein VPC mit einem public Subnet und entsprechendem Routing über einen Internet Gateway. Mit Security Gruppen erlauben wir zusätzlich Port 80 und 22 (Egress & Ingress).
* EC2 Instanz erstellen
    * Initialisieren während dem ersten starten mit "Cloud-Init".
     Cloud-Init führt folgende Schritte durch:
        * Fügt die Paketquelle für "Ansible" hinzu
        * Aktualisiert die vorhandenen Pakete
        * Setzt die Zeitzone auf "Europe/Zurich"
        * Konfiguriert NTP mit Pool/Server
        * Entfernt "snapd"
        * Installiert Pakete: python-pip, nginx, ansible, htop
        * Erstellt einfache "nginx" Standardseite

### How do I get set up? ###

* Eine detaillierte Anleitung findest du hier: https://www.supportblog.ch/aws-ec2-instanz-erstellen-mit-terraform/
